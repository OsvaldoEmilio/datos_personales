@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <h1>Personas</h1>
            <table class="table table-bordered">
            <thead class="text-light bg-dark">
                <tr>
                    <td>ID</td>
                    <td>Nombre</td>
                    <td>Apellido Paterno</td>
                    <td>Apellido Materno</td>
                    <td>Fecha de Nacimiento</td>
                    <td>Accion</td>
                </tr>
            </thead> 
            <tbody>  
                @foreach($persona as $perso)
                <tr>
                    <td>{{$perso->id}}</td>
                    <td>{{$perso->nombre}}</td>
                    <td>{{$perso->apellidopaterno}}</td>
                    <td>{{$perso->apellidomaterno}}</td>
                    <td>{{$perso->fechadenacimiento}}</td>
                    <td><a href="{{url('/personas/'.$perso->id.'/edit')}}" class="btn btn-link">Editar</a>
                    @include('personas.delete',['persona' => $persona])
                    </td>
                </tr>
                @endforeach
            </tbody>     
            </table>
            <form action="{{route('personas.create')}}" method="GET">
                <input type="submit" class="btn btn-success" value="Nuevo">
            </form>

            
        </div>
    </div>
</div>
@endsection