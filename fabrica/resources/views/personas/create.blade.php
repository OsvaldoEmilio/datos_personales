@extends('layouts.app')

@section('content')
<div class="container">
    <form action="{{route('personas.store')}}" method="POST">
    @csrf
    <div class="row justify-content-center form-group">
        <div class="col-md-8">
            <h1>Registrar Personas</h1>
            <hr>
            <label for="">Nombre</label>
            <input type="text" class="form-control" name="nombre">
            <label for="">Apellido Paterno</label>
            <input type="text" class="form-control" name="apellidopaterno">
            <label for="">Apellido Materno</label>
            <input type="text" class="form-control" name="apellidomaterno">
            <label for="">Fecha de Nacimiento</label>
            <input type="date" class="form-control" name="fechadenacimiento"><br>
            <input type="submit" class="btn btn-primary" value="Guardar">
        </div>
    </div>
    </form>
</div>
@endsection