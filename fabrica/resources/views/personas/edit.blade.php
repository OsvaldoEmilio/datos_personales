@extends('layouts.app')

@section('content')
<div class="container">
    <form action="{{route('personas.update',$persona->id)}}" method="POST">
    {{method_field('PATCH')}}
    @csrf
    <div class="row justify-content-center form-group">
        <div class="col-md-8">
            <h1>Editar Datos de Personas</h1>
            <hr>
            <label for="">Nombre</label>
            <input type="text" class="form-control" value="{{$persona->nombre}}" name="nombre">
            <label for="">Apellido Paterno</label>
            <input type="text" class="form-control" value="{{$persona->apellidopaterno}}" name="apellidopaterno">
            <label for="">Apellido Materno</label>
            <input type="text" class="form-control" value="{{$persona->apellidomaterno}}" name="apellidomaterno">
            <label for="">Fecha de Nacimiento</label>
            <input type="date" class="form-control" value="{{$persona->fechadenacimiento}}" name="fechadenacimiento"><br>
            <input type="submit" class="btn btn-primary" value="Guardar">
        </div>
    </div>
    </form>
</div>
@endsection